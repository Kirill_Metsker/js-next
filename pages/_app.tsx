import '../styles/globals.css';
import type { AppProps } from 'next/app';
import { UserWrapper } from '../context/User';
import './main.css';
// import '../components/SelectCity/SelectCity.scss';

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <UserWrapper>
      <Component {...pageProps} />;
    </UserWrapper>
  );
}
export default MyApp;
