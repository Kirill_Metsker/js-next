const getProducts = async (req: any, res: any) => {
  let products;
  await fetch(`https://reqres.in/api/products`)
    .then((response) => response.json())
    .then((json) => (products = json));
  res.status(200).json(products);
};
export default getProducts;
